from sklearn.datasets import load_iris
from sklearn.linear_model import Perceptron
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split, cross_val_score

#X, y = load_digits(return_X_y=True)
X, y = load_iris(return_X_y=True)
X_train, X_test, y_train, y_test = train_test_split(X, y)

clf = Perceptron(tol=1e-3, random_state=0)
clf = MLPClassifier(hidden_layer_sizes=(100,), max_iter=1000)
print(cross_val_score(clf, X, y, cv=5))
